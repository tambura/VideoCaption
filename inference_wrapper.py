import caption_model
import numpy as np
from MSRVTT.utils import inference_wrapper_base


class InferenceWrapper(inference_wrapper_base.InferenceWrapperBase):
  """Model wrapper class for performing inference with a ShowAndTellModel."""

  def __init__(self):
    super(InferenceWrapper, self).__init__()

  def build_model(self, model_config):
    model = caption_model.CaptionModel(model_config,
                                       mode="inference",
                                       bias_init_vector=np.load(model_config["voc_bias"]))
    model.build()
    return model

  def feed_video(self, sess, input):
    initial_state = sess.run(fetches="lstm/initial_state:0",
                             feed_dict={"video_feed:0": input})
    return initial_state

  def inference_step(self, sess, input_feed, state_feed):
    softmax_output, state_output = sess.run(
        fetches=["softmax:0", "lstm/state:0"],
        feed_dict={
            "input_feed:0": input_feed,
            "lstm/state_feed:0": state_feed,
        })
    return softmax_output, state_output, None