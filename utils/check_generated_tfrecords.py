import os
import tensorflow as tf
import numpy as np
import vocabulary
import yaml
import ipdb
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--config", type=str)
args = parser.parse_args()

with open(args.config) as f:
    config = yaml.load(f)


def load_tfrecords():
    """Check the tfrecords that contain features and captions."""
    file_names = tf.gfile.Glob(os.path.join(config["record_path"], config["input_file_pattern"]))
    dataset = tf.data.TFRecordDataset(file_names)
    dataset = dataset.map(parse_feature_tfrecord)  # Parse TFRecord.
    dataset = dataset.padded_batch(
        batch_size=128,
        padded_shapes=(tf.TensorShape([1]),
                       tf.TensorShape([config["video_feature_dim"]]),
                       tf.TensorShape([None]),
                       tf.TensorShape([None])))
    iterator = dataset.make_initializable_iterator()
    video_id, video_feature, caption, caption_idx = iterator.get_next()

    return iterator, video_id, video_feature, caption, caption_idx


def parse_feature_tfrecord(example_proto):
    """Parse the feature tfrecords. Used in the map in Dataset below."""
    context, sequence = tf.parse_single_sequence_example(
        example_proto,
        context_features={
            "video/id": tf.FixedLenFeature([1], dtype=tf.int64),
            "video/feature": tf.FixedLenFeature([config["video_feature_dim"]],
                                                dtype=tf.float32)
        },
        sequence_features={
            "video/caption": tf.FixedLenSequenceFeature([], dtype=tf.string),
            "video/caption_idx": tf.FixedLenSequenceFeature([], dtype=tf.int64)
        }
    )

    vid = context["video/id"]
    feature = context["video/feature"]
    cap = sequence["video/caption"]
    cap_idx = sequence["video/caption_idx"]

    return vid, feature, cap, cap_idx


def check_feature_tfrecord(start_video_id):
    """Check the tfrecords that contain features and captions."""
    file_names = tf.gfile.Glob(os.path.join(config["record_path"], config["input_file_pattern"]))
    dataset = tf.data.TFRecordDataset(file_names)
    dataset = dataset.map(parse_feature_tfrecord)  # Parse TFRecord.
    dataset = dataset.padded_batch(batch_size=128,
                           padded_shapes=(tf.TensorShape([1]),
                                          tf.TensorShape([config["video_feature_dim"]]),
                                          tf.TensorShape([None]),
                                          tf.TensorShape([None])))
    iterator = dataset.make_initializable_iterator()
    video_id, video_feature, caption, caption_idx = iterator.get_next()

    caption_mask = tf.cast(tf.not_equal(caption_idx, 0), dtype=tf.int32)

    vgg_feature_mean = np.load(config["feature_file"])

    sess = tf.Session()
    sess.run(iterator.initializer)

    count_record = 0
    compare_result = np.zeros([1, 2], dtype=int)
    while True:
      try:
        v_id, feature, caption_array = sess.run((video_id, video_feature, caption))
        cur_feature_nparray = np.squeeze(vgg_feature_mean[v_id - start_video_id])

        # Check the video features.
        # Compare the values inside TFRecords and npy.
        # The compare should be a [?, 1] array whose elements should be all zeros.
        compare = np.logical_not(np.equal(cur_feature_nparray, feature)).astype(int)
        compare = np.expand_dims(np.sum(compare, axis=1), axis=1)
        compare = np.hstack((v_id, compare))
        compare_result = np.vstack((compare_result, compare))

        count_record += 128
        # Print and check the captions inside TFRecords.
        if count_record % 1000 == 0:
          caption_output = np.hstack((v_id, caption_array))
          print(caption_output)
      except tf.errors.OutOfRangeError:
        np.save("check_feature_result_1117.npy", compare_result)
        np.save("check_caption_result_1117.npy", caption_output)
        print("Checked {} records.".format(count_record))
        print("There are {} values do not match.".format(int(np.sum(compare_result, axis=0)[1])))
        break


def parse_frame_tfrecord(example_proto):
    """Parse the frame TFRecords, Used in the map() in Dataset below."""
    context, sequence = tf.parse_single_sequence_example(
        example_proto,
        context_features={
            "video/id": tf.FixedLenFeature([1], dtype=tf.int64),
            "video/frame": tf.FixedLenFeature(shape=[], dtype=tf.string)
        },
        sequence_features={
            "video/caption": tf.FixedLenSequenceFeature([], dtype=tf.string),
            "video/caption_idx": tf.FixedLenSequenceFeature([], dtype=tf.int64)
        }
    )

    vid = context["video/id"]
    encoded_frame = context["video/frame"]
    cap = sequence["video/caption"]
    cap_idx = sequence["video/caption_idx"]

    return vid, encoded_frame, cap, cap_idx


def check_frame_record(record_file, log_dir):
    """
    One frame for one video. Not used.
    """
    dataset = tf.data.TFRecordDataset(record_file)
    dataset = dataset.map(parse_frame_tfrecord)

    iterator = dataset.make_initializable_iterator()
    video_id, encoded_frame, caption, caption_idx = iterator.get_next()

    frame = tf.image.decode_jpeg(encoded_frame, channels=3)
    frame = tf.expand_dims(frame, 0)
    tf.summary.image(name='frame', tensor=frame)
    merged = tf.summary.merge_all()
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)
    summary_writer = tf.summary.FileWriter(log_dir)

    with tf.Session() as sess:
        sess.run(iterator.initializer)

        for i in range(10):
            try:
                v_id, _, caption_array, summary = sess.run((video_id, frame, caption, merged))
                print("Video id: {}".format(v_id))
                print("Caption: {}".format(caption_array))
                # Check the video features.
                summary_writer.add_summary(summary, i)
            except tf.errors.OutOfRangeError:
                break


def parse_frames_tfrecord(example_proto):
    """Parse the frame TFRecords, Used in the map() in Dataset below."""
    context, sequence = tf.parse_single_sequence_example(
        example_proto,
        context_features={
            "video/id": tf.FixedLenFeature([1], dtype=tf.int64),
        },
        sequence_features={
            "video/frames": tf.FixedLenSequenceFeature(shape=[], dtype=tf.string),
            "video/caption": tf.FixedLenSequenceFeature([], dtype=tf.string),
            "video/caption_idx": tf.FixedLenSequenceFeature([], dtype=tf.int64)
        }
    )

    vid = context["video/id"]
    encoded_frames = sequence["video/frames"]
    cap = sequence["video/caption"]
    cap_idx = sequence["video/caption_idx"]

    return vid, encoded_frames, cap, cap_idx


def check_frames_record(record_file, log_dir):
    """
    Several frames for one video.
    SequenceExample in tfrecords.
    """
    dataset = tf.data.TFRecordDataset(record_file)
    dataset = dataset.map(parse_frames_tfrecord)

    iterator = dataset.make_initializable_iterator()
    video_id, encoded_frames, caption, caption_idx = iterator.get_next()

    for idx in range(10):
        frame = tf.image.decode_jpeg(encoded_frames[idx], channels=3)
        frame = tf.expand_dims(frame, 0)
        tf.summary.image(name='frame', tensor=frame)
    
    merged = tf.summary.merge_all()
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)
    summary_writer = tf.summary.FileWriter(log_dir)

    with tf.Session() as sess:
        sess.run(iterator.initializer)

        """
        for i in range(10):
            try:
                v_id, _, caption_array, summary = sess.run((video_id, frame, caption, merged))
                print("Video id: {}".format(v_id))
                print("Caption: {}".format(caption_array))
                # Check the video features.
                summary_writer.add_summary(summary, i)
            except tf.errors.OutOfRangeError:
                break
        """
        try:
            # v_id, summary = sess.run(video_id)
            (v_id,
             v_frames,
             caption_array,
             caption_idx_array,
             summary) = sess.run((video_id, frame, caption, caption_idx, merged))
            summary_writer.add_summary(summary, 0)
            print("THE VIDEO ID: ", v_id)
            print("THE CAPTION: ", caption_array)
            print("THE ENCODED CAPTION: ", caption_idx_array)
            return
        except tf.errors.OutOfRangeError:
            return


def print_max_length():
    """
    Print the maximum length of the captions in train set.
    Make sure the padding size is not less than the max length.
    Delete the very long and very rare records.
    This will affect the LSTM steps if using for loop.
    Edit the arguments of this script to change split among train, val, test.
    """
    iterator, video_id, video_feature, caption, caption_idx = load_tfrecords()
    with tf.Session() as sess:
        sess.run(iterator.initializer)

        count_records = 0
        max_len_cap = 0
        max_len_cap_idx = 0
        while True:
            try:
                v_id, feature, cap, cap_idx = sess.run((video_id, video_feature, caption, caption_idx))
                count_records += int(v_id.shape[0])
                max_len_cap = max(cap.shape[1], max_len_cap)
                max_len_cap_idx = max(cap_idx.shape[1], max_len_cap_idx)
            except tf.errors.OutOfRangeError:
                print("There are {} records in current split.".format(count_records))
                print("Max lenght of caption: {}".format(max_len_cap))
                print("Max lenght of caption in index: {}".format(max_len_cap_idx))
                break


def print_examples():
    """
    Print some records to check if the TFRecords are written correctly.
    Edit the arguments of this script to change split among train, val, test.
    :return:
        None
    """
    file_names = tf.gfile.Glob(os.path.join(config["tfrecords_root"],
                                            config["val_input_file_pattern"]))
    dataset = tf.data.TFRecordDataset(file_names)
    dataset = dataset.map(parse_feature_tfrecord)
    iterator = dataset.make_one_shot_iterator()
    vid, feat, cap, cap_idx = iterator.get_next()

    voc = vocabulary.Vocabulary(config["word_count_output_file"])

    with tf.Session() as sess:
        for i in range(5):
            video_id, feature, caption, caption_idx = sess.run((vid, feat, cap, cap_idx))
            print("Video id: ", video_id)
            print("Caption: ", caption)
            print("Feature dimension: ", feature.shape)

            caption_rec = list()
            for j in range(caption_idx.shape[0]):
                caption_rec.append(voc.id_to_word(int(caption_idx[j])))
            print("Indexed caption: ", caption_rec)


if __name__ == '__main__':
    print_examples()
    # check_frames_record("../data/test-00000-of-00001", "check_summary")
