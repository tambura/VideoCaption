import tensorflow as tf


def process_image(
        encoded_image,
        is_training,
        height,
        width,
        resize_height=346,
        resize_width=346,
        thread_id=0,
        image_format="jpeg"):
    """
    Args:
        encoded_image: String Tensor containing the image.
    """
    image = tf.image.decode_jpeg(encoded_image, channels=3)
    image = tf.image.convert_image_dtype(image, dtype=tf.float32)

    tf.summary.image("original_image", tf.expand_dims(image, 0))

    if resize_height:
        image = tf.image.resize_images(image,
                                       size=[resize_height, resize_width],
                                       method=tf.image.ResizeMethod.BILINEAR)
    if is_training:
        image = tf.random_crop(image, [height, width, 3])
    else:
        image = tf.image.resize_image_with_crop_or_pad(image, height, width)

    tf.summary.image("resized_image", tf.expand_dims(image, 0))

    image = tf.subtract(image, 0.5)
    image = tf.multiply(image, 2.0)

    tf.summary.image("final_image", tf.expand_dims(image, 0))

    return image
