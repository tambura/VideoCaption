import json
import numpy as np
from collections import namedtuple
import yaml


VideoMeatadata = namedtuple('VideoMetadata',
                            ['video_id', 'caption'])


class Vocabulary(object):
    """Simple vocabulary wrapper."""

    def __init__(self, voc_json_path):
        """Initializes the vocabulary.

        Args:
        word_to_id: A dictionary of word to word_id.
        unk_id: Id of the special 'unknown' word.
        """
        with open(voc_json_path) as f:
            voc_dict = json.load(f)
        voc_dict["id_to_word"] = {int(k): v for k, v in voc_dict["id_to_word"].items()}
        self._word_to_id = voc_dict["word_to_id"]
        self._id_to_word = voc_dict["id_to_word"]
        self._unk_id = voc_dict["unkown_id"]
        self.start_id = voc_dict["word_to_id"]["<BOS>"]
        self.end_id = voc_dict["word_to_id"]["<EOS>"]
        self.unk_id = voc_dict["unkown_id"]

    def word_to_id(self, word):
        """Returns the integer id of a word string."""
        if word in self._word_to_id:
            return self._word_to_id[word]
        else:
            return self._unk_id

    def id_to_word(self, idx):
        return self._id_to_word[idx]


class DataProvider(object):
    """
    Load json metadata and split it into trian, val and test.
    Create a vocabulary from train set.
    """
    def __init__(self, json_path):
        """A helper class to process dataset metadata in json."""
        print('Initializing data provider')

        self.dataset_meta = json.load(open(json_path, 'r'))
        self.splited_sent_meta = None
        self.sent_iter = None
        self.word_to_ix = None
        self.ix_to_word = None
        self.bias_init_vector = None

    def split(self, train_amt, val_amt, test_amt):
        self.splited_sent_meta = {'train': list(), 'val': list(), 'test': list()}

        for sentence_meta in self.dataset_meta['sentences']:
            video_id = int(sentence_meta['video_id'][5:])
            if video_id in range(train_amt):
                self.splited_sent_meta['train'].append(VideoMeatadata(video_id,
                                                                      sentence_meta['caption']))
            if video_id in range(train_amt, train_amt + val_amt):
                self.splited_sent_meta['val'].append(VideoMeatadata(video_id,
                                                                    sentence_meta['caption']))
            if video_id in range(train_amt + val_amt, train_amt + val_amt + test_amt):
                self.splited_sent_meta['test'].append(VideoMeatadata(video_id,
                                                                     sentence_meta['caption']))

    def iter_sentences(self, split='train'):
        for sent_meta in self.splited_sent_meta[split]:
            yield sent_meta.caption

    def build_word_vector(self, voc_json_path, voc_bias_json_path, word_count_threshold):
        word_counts = dict()
        count_sentence = 0
        for sent in self.iter_sentences():
            count_sentence += 1
            words = sent.split()
            for word in words:
                word_counts[word] = word_counts.get(word, 0) + 1
        vocab = [w for w in word_counts if word_counts[w] >= word_count_threshold]
        print('Build vocabulary with size: {}'.format(len(vocab)))

        word_counts["<UKN>"] = 0
        with open("word_count.txt", 'w') as f:
            for word, frequency in word_counts.items():
                if frequency >= word_count_threshold:
                    f.write("{} {}\n".format(word, frequency))
                else:
                    word_counts["<UKN>"] += frequency

        # with K distinct words:
        # - there are K+1 possible inputs (START token and all the words)
        # - there are K+1 possible outputs (END token and all the words)
        # we use ixtoword to take predicted indeces and map them to words for output visualization
        # we use wordtoix to take raw words and get their index in word vector matrix
        ixtoword = dict()
        ixtoword[0] = '<EOS>'  # period at the end of the sentence. make first dimension be end token
        ixtoword[9] = '<BOS>'
        ixtoword[2] = '<UKN>'

        wordtoix = dict()
        wordtoix['<EOS>'] = 0  # make first vector be the start token
        wordtoix['<BOS>'] = 1
        wordtoix["<UKN>"] = 2
        ix = 3
        for w in vocab:
            wordtoix[w] = ix
            ixtoword[ix] = w
            ix += 1
        # compute bias vector, which is related to the log probability of the distribution
        # of the labels (words) and how often they occur. We will use this vector to initialize
        # the decoder weights, so that the loss function doesnt show a huge increase in performance
        # very quickly (which is just the network learning this anyway, for the most part). This makes
        # the visualizations of the cost function nicer because it doesn't look like a hockey stick.
        # for example on Flickr8K, doing this brings down initial perplexity from ~2500 to ~170.
        word_counts['<BOS>'] = word_counts['<EOS>'] = count_sentence
        bias_init_vector = np.array([1.0 * word_counts[ixtoword[i]] for i in ixtoword])
        bias_init_vector /= np.sum(bias_init_vector)  # normalize to frequencies
        bias_init_vector = np.log(bias_init_vector)
        bias_init_vector -= np.max(bias_init_vector)  # shift to nice numeric range

        self.word_to_ix = wordtoix
        self.ix_to_word = ixtoword
        self.bias_init_vector = bias_init_vector

        voc_dict = dict()
        voc_dict["word_to_id"] = self.word_to_ix
        voc_dict["id_to_word"] = self.ix_to_word
        voc_dict["unkown_id"] = 2
        with open(voc_json_path, 'w') as f:
            json.dump(voc_dict, f)

        np.save(voc_bias_json_path, bias_init_vector)

        vocabulary = Vocabulary(voc_json_path)

        return vocabulary

    def count_max_length(self):
        max_length = 0
        for sentence in self.iter_sentences('train'):
            length = len(sentence.split(' '))
            max_length = length if length > max_length else max_length
        print('The max length of the sentences in training is: {}'.format(max_length))


    def get_test_captions(self):
        with open("reference.txt", 'a') as target:
            for meta in self.splited_sent_meta["test"]:
                id = getattr(meta, "video_id")
                cap = getattr(meta, "caption")
                target.write("{} {}\n".format(id, cap))


if __name__ == '__main__':
    with open("/home/jiangchen/data/tensorflow/jointly/msrvtt_fc/config.yaml") as f:
        config = yaml.load(f)

    dp = DataProvider(config['msrvtt_meta_json'])
    dp.split(config['num_train'], config['num_val'], config['num_test'])
    dp.build_word_vector(config['voc_json'],config['voc_bias'], config['voc_threshold'])

    """
    train = dict()
    for meta in dp.splited_sent_meta["train"]:
        id = getattr(meta, "video_id")
        cap = getattr(meta, "caption")
        if id in train:
            train[id].append(cap)
        else:
            train[id] = list()
            train[id].append(cap)
    with open("train_captions.pkl", 'wb') as t:
        pickle.dump(train, t)

    val = dict()
    for meta in dp.splited_sent_meta["val"]:
        id = getattr(meta, "video_id")
        cap = getattr(meta, "caption")
        if id in val:
            val[id].append(cap)
        else:
            val[id] = list()
            val[id].append(cap)
    with open("val_captions.pkl", 'wb') as t:
        pickle.dump(val, t)

    test = dict()
    for meta in dp.splited_sent_meta["test"]:
        id = getattr(meta, "video_id")
        cap = getattr(meta, "caption")
        if id in test:
            test[id].append(cap)
        else:
            test[id] = list()
            test[id].append(cap)
    with open("test_captions.pkl", 'wb') as t:
        pickle.dump(test, t)
    """
