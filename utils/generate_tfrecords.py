# -*- coding: utf-8 -*-
# The function of this file:
# Parse json, write frames and captions into TFRecords.
# Parse json, write feature and captions into TFRecords.
import ipdb
import tensorflow as tf
import numpy as np

import os
import data_provider
import vocabulary
import threading
import random
import h5py
import json
from datetime import datetime
from collections import Counter
from collections import namedtuple
import argparse
import yaml


parser = argparse.ArgumentParser()
parser.add_argument("--config", type=str)
args = parser.parse_args()

with open(args.config) as f:
    config = yaml.load(f)


VideoMeatadata = namedtuple('VideoMetadata',
                            ['video_id', 'caption'])


class Vocabulary(object):
    """Simple vocabulary wrapper."""

    def __init__(self, vocab, unk_id):
        """Initializes the vocabulary.

        Args:
        vocab: A dictionary of word to word_id.
        unk_id: Id of the special 'unknown' word.
        """
        self._vocab = vocab
        self._unk_id = unk_id

    def word_to_id(self, word):
        """Returns the integer id of a word string."""
        if word in self._vocab:
            return self._vocab[word]
        else:
            return self._unk_id


class FrameDecoder(object):
    def __init__(self):
        # Create a single TensorFlow Session for all image decoding calls.
        self._sess = tf.Session()

        # TensorFlow ops for JPEG decoding.
        self._encoded_jpeg = tf.placeholder(dtype=tf.string)
        self._decode_jpeg = tf.image.decode_jpeg(self._encoded_jpeg, channels=3)

    def decode_jpeg(self, encoded_jpeg):
        image = self._sess.run(self._decode_jpeg,
                               feed_dict={self._encoded_jpeg: encoded_jpeg})
        assert len(image.shape) == 3
        assert image.shape[2] == 3
        return image


def _int64_feature(value):
    """Wrapper for inserting an int64 Feature into a SequenceExample proto."""
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def _bytes_feature(value):
    """Wrapper for inserting a image bytes Feature into a SequenceExample proto."""
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def _bytes_feature_string(value):
    """Wrapper for inserting a string bytes Feature into a SequenceExample proto."""
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value.encode('utf-8')]))


def _float_feature(value):
    """Wrapper for inserting a float Feature into a SequenceExample proto."""
    return tf.train.Feature(float_list=tf.train.FloatList(value=value))


def _int64_feature_list(values):
    """Wrapper for inserting an int64 FeatureList into a SequenceExample proto."""
    return tf.train.FeatureList(feature=[_int64_feature(v) for v in values])


def _string_bytes_feature_list(values):
    """Wrapper for inserting a string bytes FeatureList into a SequenceExample proto."""
    return tf.train.FeatureList(feature=[_bytes_feature(v.encode('utf-8')) for v in values])


def _image_bytes_feature_list(values):
    """Wrapper for inserting a image bytes FeatureList into a SequenceExample proto."""
    return tf.train.FeatureList(feature=[_bytes_feature(v) for v in values])


def _frame_to_sequence_example(sent_meta, decoder, frame, vocab):
    """Write video frames and caption pair into TFRecords.
    Sample one frame for one caption.
    The caption in sent_meta and frame pair should be made randomly before calling this function.

    Args:
      sent_meta: A Sentence Metadata object.
      decoder: A image decoder.
      frame: A frame file name.
      vocab: A Vocabulary object.

    Returns:
      A SequenceExample proto.
    """
    with tf.gfile.FastGFile(frame, "rb") as f:
        encoded_frame = f.read()

    try:
        decoder.decode_jpeg(encoded_frame)
    except (tf.errors.InvalidArgumentError, AssertionError):
        print("Skipping file with invalid JPEG data: %s" % frame)
        return

    context = tf.train.Features(feature={
        "video/id": _int64_feature(sent_meta.video_id),
        "video/frame": _bytes_feature(encoded_frame),
    })

    # TODO Use new vocabulary.
    caption = sent_meta.caption.split(' ')
    caption_idx = [vocab.word_to_id(word) for word in caption]
    caption_idx.insert(0, 1)  # Insert the start token of sequence.
    caption_idx.append(0)  # Append the end token of sequence.
    feature_lists = tf.train.FeatureLists(
        feature_list={
            "video/caption": _string_bytes_feature_list(caption),
            "video/caption_idx": _int64_feature_list(caption_idx)})

    sequence_example = tf.train.SequenceExample(
        context=context, feature_lists=feature_lists)

    return sequence_example


def _frames_to_sequence_example(sent_meta, decoder, frames, vocab):
    """Write video frames and caption pair into TFRecords.
    Sample several frames for one caption.

    Args:
      sent_meta: A Sentence Metadata object.
      decoder: A image decoder.
      frames: A list of encoded frames file names.
      vocab: A Vocabulary object.

    Returns:
      A SequenceExample proto.
    """
    encoded_frames = list()
    for frame in frames:
        with tf.gfile.FastGFile(frame, "rb") as f:
            encoded_frame = f.read()

        try:
            decoder.decode_jpeg(encoded_frame)
        except (tf.errors.InvalidArgumentError, AssertionError):
            print("Skipping file with invalid JPEG data: %s" % frame)
            return

        encoded_frames.append(encoded_frame)

    context = tf.train.Features(feature={
        "video/id": _int64_feature(sent_meta.video_id),
    })

    # TODO Use new vocabulary.
    caption = sent_meta.caption.split(' ')
    caption_idx = [vocab.word_to_id(word) for word in caption]
    caption_idx.insert(0, 1)  # Insert the start token of sequence.
    caption_idx.append(0)  # Append the end token of sequence.

    feature_lists = tf.train.FeatureLists(
        feature_list={
            "video/frames": _image_bytes_feature_list(encoded_frames),
            "video/caption": _string_bytes_feature_list(caption),
            "video/caption_idx": _int64_feature_list(caption_idx)})

    sequence_example = tf.train.SequenceExample(
        context=context, feature_lists=feature_lists)

    return sequence_example


def _feature_to_sequence_example(metadata, feature, vocab):
    """
    Write features and captions into TFRecords.
    :param metadata: caption list with one word as one element.
    :param feature: feature for the current video.
    :param vocab: word to index.
    :return:
    """
    caption_list = metadata.caption
    caption_idx = [vocab.word_to_id(word) for word in caption_list]

    context = tf.train.Features(feature={
        "video/id": _int64_feature(metadata.video_id),
        "video/feature": _float_feature(feature)})

    feature_lists = tf.train.FeatureLists(feature_list={
        "video/caption": _string_bytes_feature_list(caption_list),
        "video/caption_idx": _int64_feature_list(caption_idx)})

    sequence_example = tf.train.SequenceExample(
        context=context, feature_lists=feature_lists)

    return sequence_example


def load_feature(feature_category, feature_file, video_id):
    """
    Load feature according to feature category.
    :param feature_category: vgg19_mean, res101_fc_concat, res101_fc_mean, res101_box_concat
    :param video_id: If feature file is separated files, use id to load correct one.
    :return: The loaded feature.
    """
    if feature_category == "vgg_mean":
        return None  # Never use this.
    # concatenate the features of different frames.
    elif feature_category == "res101_fc_concat":
        with open(config["sampled_frames"]) as f:
            sampled_frames = json.load(f)
        frame_ids = sampled_frames["{:04d}".format(video_id)]  # video_id is "0010"
        frames = list()
        for f_id in frame_ids:
            feature_path = os.path.join(config["feature_root"], "{:04d}_{}.npy".format(video_id, f_id))
            frames.append(np.load(feature_path))
        video_feature = np.concatenate(frames)

        return video_feature
    # Mean pool the features of different frames.
    elif feature_category == "res101_fc_mean":  
        with open(config["sampled_frames"]) as f:
            sampled_frames = json.load(f)
        frame_ids = sampled_frames["{:04d}".format(video_id)]  # video_id is "0010"
        features = list()
        for f_id in frame_ids:
            feature_path = os.path.join(config["feature_root"], "{:04d}_{}.npy".format(video_id, f_id))
            features.append(np.load(feature_path))
        feature = np.divide(np.sum(features, axis=0), len(features))  # Mean feature.
        
        return feature
    # bottom up attention by fast rcnn.
    elif feature_category == "res101_att":
        return None
    elif feature_category == "inception_concat":
        feature_path = os.path.join(config["feature_root"], "video{:04d}.npy".format(video_id))
        feature = np.load(feature_path)
        return feature
    elif feature_category == "resnet200":
        return feature_file[str(video_id)][()]  # hdf5 to numpy array
    elif feature_category == "resnet_temporal_mean":
        feature = feature_file["{:04d}".format(video_id)][()]  # key: "0000"
        feature = np.mean(feature, axis=0)
        return feature
    elif feature_category == "densenet_concat":
        feature_path = os.path.join(config["feature_file"], "video{:04d}.npy".format(video_id))
        feature = np.load(feature_path)
        feature = np.reshape(feature, [-1])  # concatenate the feature of each frame.
        return feature

def _load_and_process_video_info(video_data_info, start_id, end_id):
    with open(video_data_info, 'r') as f:
        video_data_info = json.load(f)

    def delete_non_ascii(caption):
        caption = caption.replace(u'\xe9', 'e')
        caption = caption.replace(u'\u2019', "'")
        caption = caption.replace(u'\u0432', 'b')
        return caption

    video_meta_data = list()
    for caption_meta in video_data_info["sentences"]:
        video_id = int(caption_meta['video_id'][5:])
        if video_id in range(start_id, end_id):
            caption = delete_non_ascii(caption_meta["caption"])
            caption_list = caption.split()
            caption_list.insert(0, "<S>")
            caption_list.append("</S>")

            video_meta_data.append(VideoMeatadata(video_id, caption_list))

    return video_meta_data


def _create_vocab(captions):
    """Creates the vocabulary of word to word_id.

    The vocabulary is saved to disk in a text file of word counts. The id of each
    word in the file is its corresponding 0-based line number.

    Args:
    captions: A list of lists of strings.

    Returns:
    A Vocabulary object.
    """
    print("Creating vocabulary.")
    counter = Counter()
    counter.update(captions)
    print("Total words:", len(counter))

    # Filter uncommon words and sort by descending count.
    word_counts = [x for x in counter.items() if x[1] >= config["word_count_threshold"]]
    word_counts.sort(key=lambda x: x[1], reverse=True)
    print("Words in vocabulary:", len(word_counts))

    # Write out the word counts file.
    with tf.gfile.FastGFile(config["word_count_output_file"], "w") as f:
        f.write("\n".join(["%s %d" % (w, c) for w, c in word_counts]))
    print("Wrote vocabulary file:", config["word_count_output_file"])

    # Create the vocabulary dictionary.
    reverse_vocab = [x[0] for x in word_counts]
    unk_id = len(reverse_vocab)
    vocab_dict = dict([(x, y) for (y, x) in enumerate(reverse_vocab)])
    vocab = Vocabulary(vocab_dict, unk_id)

    return vocab


def _process_video_file(thread_index,
                        ranges,
                        feature_file,
                        split_name,
                        sentences_meta,
                        tfrecords_mode,
                        vocab, num_shards):
    """
    Write dataset into tfrecords.
    :param thread_index: 线程索引号
    :param ranges: 每个shard中的起始和终止索引
    :param split_name: one of 'train', 'val', 'test'
    :param sentences_meta: 某个数据子集的所有caption的metadata
    :param tfrecords_mode: 指定TFRecord的类别
    :param start_id: 某个子集第一个视频的id，因为npy文件中始终从0开始按行号取特征
    :param vocab: 由训练集得到的词表
    :param num_shards: 将数据子集分为几个TFRecord文件
    :return:
    """
    num_threads = len(ranges)
    assert not num_shards % num_threads
    num_shards_per_thread = int(num_shards / num_threads)
    num_videos_per_thread = ranges[thread_index][1] - ranges[thread_index][0]

    shard_ranges = np.linspace(
            ranges[thread_index][0],
            ranges[thread_index][1],
            num_shards_per_thread + 1).astype(int)

    # Only used in vgg_mean feature. Features for all the videos are in one file.
    features = None
    if tfrecords_mode == "vgg_mean":
        features = np.load(config["feature_root"])

    count_video_in_thread = 0
    for s in range(num_shards_per_thread):  # 一次循环处理一个shard
        # s是shard的编号，从0到31，共32个，每个线程要处理32个shards
        # Generate a sharded version of the file name, e.g. 'train-00002-of-00010'
        # 每个线程/每个batch，处理32个shards，每个shard内部有若干图片，根据train、val、test的数量变动
        print("Now video: ", s)
        shard = thread_index * num_shards_per_thread + s
        if not os.path.exists(config["tfrecords_root"]):
            os.makedirs(config["tfrecords_root"])
        output_filename = "%s-%.5d-of-%.5d" % (split_name, shard, num_shards)
        output_file = os.path.join(config["tfrecords_root"], output_filename)
        writer = tf.python_io.TFRecordWriter(output_file)

        frames_decoder = FrameDecoder()
        count_video_in_shard = 0
        videos_in_shard = np.arange(shard_ranges[s], shard_ranges[s + 1], dtype=int)
        for i in videos_in_shard:  # 一次循环处理一个video
            sent_meta = sentences_meta[i]
            video_id = sent_meta.video_id
            example = None

            if tfrecords_mode == 'frame':
                frame_id = random.randrange(0, 10)
                frame_name = os.path.join(
                        config["frame_root"],
                        'video{:04d}'.format(video_id),
                        '{}.jpg'.format(frame_id))
                example = _frame_to_sequence_example(sent_meta, frames_decoder, frame_name, vocab)
            if tfrecords_mode == 'frames':
                frame_path = os.path.join(config["frame_root"], 'video{:04d}'.format(video_id))
                frame_names = sorted(tf.gfile.Glob(frame_path + '/*'))
                example = _frames_to_sequence_example(sent_meta, frames_decoder, frame_names, vocab)
            if tfrecords_mode == 'vgg_mean':
                feature = features[video_id, :]  # 4096
                example = _feature_to_sequence_example(sent_meta, feature, vocab)
            if tfrecords_mode == "res101_fc_concat":
                feature = load_feature(tfrecords_mode, None, sent_meta.video_id)
                example = _feature_to_sequence_example(sent_meta, feature, vocab)
            if tfrecords_mode == "inception_concat":
                feature = load_feature(tfrecords_mode, None, sent_meta.video_id)
                example = _feature_to_sequence_example(sent_meta, feature, vocab)
            if tfrecords_mode == "resnet200":
                feature = load_feature(tfrecords_mode, feature_file, sent_meta.video_id)
                example = _feature_to_sequence_example(sent_meta, feature, vocab)
            if tfrecords_mode == "densenet_concat":
                feature = load_feature(tfrecords_mode, feature_file, sent_meta.video_id)
                example = _feature_to_sequence_example(sent_meta, feature, vocab)
            if tfrecords_mode == "resnet_temporal_mean":
                feature = load_feature(tfrecords_mode, feature_file, sent_meta.video_id)
                example = _feature_to_sequence_example(sent_meta, feature, vocab)

            if example is not None:
                writer.write(example.SerializeToString())
                count_video_in_shard += 1  # 统计一个shard内的视频数量
                count_video_in_thread += 1  # 统计一个thread处理的视频数量
            if not count_video_in_thread % 1000:
                print("%s [thread %d]: Processed %d of %d items in thread batch." %
                      (datetime.now(), thread_index, count_video_in_thread, num_videos_per_thread))
                os.sys.stdout.flush()

        writer.close()
        print("%s [thread %d]: Wrote %d video-caption pairs to %s" %
              (datetime.now(), thread_index, count_video_in_shard, output_file))
        os.sys.stdout.flush()

    print("%s [thread %d]: Wrote %d video-caption pairs to %d shards." %
          (datetime.now(), thread_index, count_video_in_thread, num_shards_per_thread))
    os.sys.stdout.flush()


def _process_dataset(split_name, captions_meta, vocab, num_shards, tfrecords_mode):
    """
    Split data into several shards and start multiple threads to call the _process_video_file.
    :param split_name: 'train', 'val', 'test'
    :param captions_meta: metadata of the current split.
    :param vocab: vocabulary object.
    :param num_shards: Integer number of shards for the output TFRecords files.
    :param mode: 'frame' or 'feature'
    :return:
    """
    # Shuffle
    random.seed(12345)
    random.shuffle(captions_meta)

    # Break the videos into num_threads batches. Batch i is defined as
    # videos[ranges[i][0]:ranges[i][1]].
    num_threads = min(num_shards, config["thread_amount"])
    # 生成array，含有num_threads个元素，元素间隔相同
    spacing = np.linspace(0, len(captions_meta), num_threads + 1).astype(np.int)
    ranges = []
    threads = []
    for i in range(len(spacing) - 1):
        ranges.append([spacing[i], spacing[i + 1]])  # 每个间隔的起始0-100,100-200，...

    feature_file = None
    if split_name == "train" and config["input_feature_mode"] == "hdf5":
        feature_file = h5py.File(config["train_feature_file"], 'r')
    elif split_name == "val"and config["input_feature_mode"] == "hdf5":
        feature_file = h5py.File(config["val_feature_file"], 'r')

    # Create a mechanism for monitoring when all threads are finished.
    coord = tf.train.Coordinator()

    # Launch a thread for each batch.
    print("Launching %d threads for spacings: %s" % (num_threads, ranges))
    for thread_index in range(len(ranges)):  # python多线程的方法
        arguments = (thread_index,
                     ranges,
                     feature_file,
                     split_name,
                     captions_meta,
                     tfrecords_mode,
                     vocab,
                     num_shards)
        t = threading.Thread(target=_process_video_file, args=arguments)
        t.start()
        threads.append(t)

    # Wait for all the threads to terminate.
    coord.join(threads)

    if not feature_file is None:
        feature_file.close()
    print("%s: Finished processing all %d video-caption pairs in data set '%s'." %
          (datetime.now(), len(captions_meta), split_name))


def generate_tiny():
    """
    Generate a sample tfrecord to check if the code is right.
    """
    captions_meta = data_provider.DataProvider(config["msrvtt_meta_json"])
    captions_meta.split(6000, 1000, 1000)
    vocab = captions_meta.build_word_vector("tmp_voc.json", word_count_threshold=10)

    if not os.path.exists(config["tfrecords_root"]):
        os.makedirs(config["tfrecords_root"])
    output_filename = "%s-%.5d-of-%.5d" % ("train", 0, 1)
    output_file = os.path.join(config["tfrecords_root"], output_filename)
    writer = tf.python_io.TFRecordWriter(output_file)

    frames_decoder = FrameDecoder()
    frame_path = os.path.join(config["frame_root"], 'video0000')  # frame和sent_meta不一致
    frame_names = sorted(tf.gfile.Glob(frame_path + '/*'))

    sent_meta = captions_meta.splited_sent_meta["train"][0]
    example = _frames_to_sequence_example(sent_meta, frames_decoder, frame_names, vocab)

    writer.write(example.SerializeToString())
    writer.close()


def main():
    """
    msrvtt_train_dataset = _load_and_process_video_info(config["msrvtt_meta_json"],
                                                        config["train_start_id"],
                                                        config["train_end_id"])
    """
    msrvtt_val_dataset = _load_and_process_video_info(config["msrvtt_meta_json"],
                                                      config["val_start_id"],
                                                      config["val_end_id"])
    """
    train_captions = [c for video in msrvtt_train_dataset for c in video.caption]
    vocab = _create_vocab(train_captions)

    _process_dataset("train",
                     msrvtt_train_dataset,
                     vocab,
                     config["train_shards"],
                     config["tfrecords_mode"])
    """
    vocab = vocabulary.Vocabulary(config["word_count_output_file"])
    _process_dataset("val",
                     msrvtt_val_dataset,
                     vocab,
                     config["val_shards"],
                     config["tfrecords_mode"])


if __name__ == '__main__':
    main()
    """
    generate_tiny()
    """
