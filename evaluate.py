import os
import math
import os.path
import time
import yaml

import numpy as np
import tensorflow as tf

import caption_model
from utils import vocabulary

with open("config.yaml") as stream:
    config = yaml.load(stream)

tf.logging.set_verbosity(tf.logging.INFO)


def evaluate_model(sess, model, global_step, summary_writer, summary_op, vocabulary):
    """Computes perplexity-per-word over the evaluation dataset.

    Summaries and perplexity-per-word are written out to the eval directory.

    Args:
      sess: Session object.
      model: Instance of ShowAndTellModel; the model to evaluate.
      global_step: Integer; global step of the model checkpoint.
      summary_writer: Instance of FileWriter.
      summary_op: Op for generating model summaries.
    """
    # Log model summaries on a single batch.
    summary_str = sess.run(summary_op)
    summary_writer.add_summary(summary_str, global_step)

    # Compute perplexity over the entire dataset.
    num_eval_batches = int(
        math.ceil(config['num_eval_examples'] / config['batch_size']))

    start_time = time.time()
    sum_losses = 0.
    sum_weights = 0.
    for i in range(num_eval_batches):
        cross_entropy_losses, caption_mask, vid, gt, outputs = sess.run([
            model.target_cross_entropy_losses,
            model.caption_mask,
            model.video_id,
            model.caption,
            model.to_print
        ])

        sum_losses += np.sum(cross_entropy_losses * caption_mask)
        sum_weights += np.sum(caption_mask)

        if not i % 100:
            tf.logging.info("Computed losses for %d of %d batches.", i + 1,
                            num_eval_batches)

            sent = np.argmax(outputs, axis=2)  # b x t
            for j in range(2):
                caption = list()
                for word_idx in list(sent[j]):
                    caption.append(vocabulary.id_to_word(word_idx))
                    if word_idx == vocabulary.word_to_id("</S>"):
                        break
                print(vid[j])
                print("Ground Truth: ", gt[j])
                print("Generated: ", caption)
                print("**********")

    eval_time = time.time() - start_time

    perplexity = math.exp(sum_losses / sum_weights)
    tf.logging.info("Perplexity = %f (%.2g sec)", perplexity, eval_time)

    # Log perplexity to the FileWriter.
    summary = tf.Summary()
    value = summary.value.add()
    value.simple_value = perplexity
    value.tag = "Perplexity"
    summary_writer.add_summary(summary, global_step)

    # Write the Events file to the eval directory.
    summary_writer.flush()
    tf.logging.info("Finished processing evaluation at global step %d.",
                    global_step)


def run_once(model, saver, summary_writer, summary_op, vocab):
    """Evaluates the latest model checkpoint.

    Args:
      model: Instance of ShowAndTellModel; the model to evaluate.
      saver: Instance of tf.train.Saver for restoring model Variables.
      summary_writer: Instance of FileWriter.
      summary_op: Op for generating model summaries.
    """
    model_path = tf.train.latest_checkpoint(config['checkpoint_dir'])
    if not model_path:
        tf.logging.info("Skipping evaluation. No checkpoint found in: %s",
                        config['checkpoint_dir'])
        return

    with tf.Session() as sess:
        # Load model from checkpoint.
        tf.logging.info("Loading model from checkpoint: %s", model_path)
        saver.restore(sess, model_path)
        global_step = tf.train.global_step(sess, model.global_step.name)
        tf.logging.info("Successfully loaded %s at global step = %d.",
                        os.path.basename(model_path), global_step)
        if global_step < config['min_global_step']:
            tf.logging.info("Skipping evaluation. Global step = %d < %d", global_step,
                            config['min_global_step'])
            return

        # Start the queue runners.
        coord = tf.train.Coordinator()
        # threads = tf.train.start_queue_runners(coord=coord)
        sess.run(model.iterator.initializer)

        # Run evaluation on the latest checkpoint.
        try:
            evaluate_model(
                sess=sess,
                model=model,
                global_step=global_step,
                summary_writer=summary_writer,
                summary_op=summary_op,
                vocabulary=vocab)
        except Exception as e:  # pylint: disable=broad-except
            tf.logging.error("Evaluation failed.")
            coord.request_stop(e)

        coord.request_stop()
        # coord.join(threads, stop_grace_period_secs=10)


def run():
    """Runs evaluation in a loop, and logs summaries to TensorBoard."""
    # Create the evaluation directory if it doesn't exist.
    eval_dir = config['eval_dir']
    if not tf.gfile.IsDirectory(eval_dir):
        tf.logging.info("Creating eval directory: %s", eval_dir)
        tf.gfile.MakeDirs(eval_dir)

    g = tf.Graph()
    with g.as_default():
        # Build the model for evaluation.
        model = caption_model.CaptionModel(
                    config,
                    config["eval_mode"])
        model.build()

        # Create the Saver to restore model Variables.
        saver = tf.train.Saver()

        # Create the summary operation and the summary writer.
        summary_op = tf.summary.merge_all()
        summary_writer = tf.summary.FileWriter(eval_dir)

        g.finalize()

        # Prepare vocabulary to show the result during training.

        vocab = vocabulary.Vocabulary(config["word_counts_output_file"])
        # Run a new evaluation run every eval_interval_secs.
        while True:
            start = time.time()
            tf.logging.info("Starting evaluation at " + time.strftime(
                "%Y-%m-%d-%H:%M:%S", time.localtime()))
            run_once(model, saver, summary_writer, summary_op, vocab)
            time_to_next_eval = start + config['eval_interval_secs'] - time.time()
            if time_to_next_eval > 0:
                time.sleep(time_to_next_eval)


def main(unused_argv):
    run()


if __name__ == "__main__":
    tf.app.run()
