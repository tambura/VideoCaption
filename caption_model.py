# -*- coding: utf-8 -*-
import os
import math
import tensorflow as tf
import numpy as np
from utils import video_embedding
from utils import image_processing


class CaptionModel(object):

    def __init__(self, config, mode, bias_init_vector=None):
        self.config = config
        self.mode = mode

        self.input_mode = None
        self.init_fn = None
        self.initializer = tf.random_uniform_initializer(
            minval=-self.config['initializer_scale'],
            maxval=self.config['initializer_scale'])
        # The data in the tfrecords.
        self.iterator = None
        self.video_id = None
        self.video_feature = None
        self.frames = None
        self.caption = None
        self.input_caption_idx = None
        self.target_caption_idx = None

        # The mask for the caption. Used in calculating loss.
        self.caption_mask = None

        # Google word embedding.
        self.word_emb = None
        self.word_emb_b = None
        self.seq_embedding = None

        # Video embedding.
        self.inception_variables = None
        self.video_emb_W = None
        self.video_emb_b = None

        self.concated_frame_feature = None
        self.video_emb = None
        self.lstm_cell = None

        # Affine transfer from hidden state to logits word.
        self.word_affine_W = tf.Variable(
            tf.random_uniform(
                shape=[self.config['lstm_num_units'], self.config['voc_size']],
                minval=-0.1,
                maxval=0.1),
            name='word_affine_W')
        if bias_init_vector is not None:
            self.word_affine_b = tf.Variable(
                initial_value=bias_init_vector.astype(np.float32),
                name='word_affine_b')
        else:
            self.word_affine_b = tf.Variable(
                initial_value=tf.zeros(tf.TensorShape(self.config['voc_size'])),
                name='word_affine_b')

        # Save these ops. Used in evaluate.
        # output_eval is the output of LSTM in one batch. The element is word index.
        self.output_eval = None
        self.target_cross_entropy_losses = None

        # Pass loss to train.py
        self.total_loss = None

        self.global_step = None

        self.generated_caption = None

    def build_inputs_from_feature(self):
        """
        Build the input pipeline that consume TFRecords which contain features and captions.
        :return:
        """
        def parse_feature_tfrecord(example_proto):
            """Parse tfrecords that contain frames and captions."""
            context, sequence = tf.parse_single_sequence_example(
                example_proto,
                context_features={
                    "video/id": tf.FixedLenFeature([1], dtype=tf.int64),
                    "video/feature": tf.FixedLenFeature([self.config['video_feature_dim']],
                                                        dtype=tf.float32)
                },
                sequence_features={
                    "video/caption": tf.FixedLenSequenceFeature([], dtype=tf.string),
                    "video/caption_idx": tf.FixedLenSequenceFeature([], dtype=tf.int64)
                }
            )

            vid = context["video/id"]
            feature = context["video/feature"]
            cap = sequence["video/caption"]
            cap_idx = sequence["video/caption_idx"]

            # Handle the start token and end token in input sequence and ground truth sequence.
            input_length = tf.expand_dims(tf.subtract(tf.shape(cap)[0], 1), 0)
            input_cap = tf.slice(cap_idx, [0], input_length)
            target_cap = tf.slice(cap_idx, [1], input_length)
            indicator = tf.ones(input_length, dtype=tf.int32)

            return vid, feature, cap, input_cap, target_cap, indicator

        if self.mode == "inference":
            input_feed = tf.placeholder(dtype=tf.float32,
                                        shape=[self.config["video_feature_dim"]],
                                        name="video_feed")
            caption_feed = tf.placeholder(dtype=tf.int64,
                                          shape=[None],
                                          name="input_feed")

            video_feature = tf.expand_dims(input_feed, 0)
            input_caption_idx = tf.expand_dims(caption_feed, 1)

            iterator = None
            video_id = None
            caption = None
            target_caption_idx = None
            mask = None

        else:
            if self.mode == 'train':
                file_names = tf.gfile.Glob(os.path.join(
                    self.config['tfrecords_root'],
                    self.config['train_input_file_pattern']))
            else:
                file_names = tf.gfile.Glob(os.path.join(
                    self.config["tfrecord_path"],
                    self.config["val_input_file_pattern"]))

            dataset = tf.data.TFRecordDataset(file_names)
            dataset = dataset.map(parse_feature_tfrecord)  # Parse TFRecord.
            dataset = dataset.repeat(self.config['num_epochs'])
            dataset = dataset.padded_batch(
                          batch_size=self.config['batch_size'],
                          padded_shapes=(([1],
                                          [self.config["video_feature_dim"]],
                                          [None],
                                          [None],
                                          [None],
                                          [None])))
            iterator = dataset.make_initializable_iterator()
            (video_id,
             video_feature,
             caption,
             input_caption_idx,
             target_caption_idx,
             mask) = iterator.get_next()

        self.iterator = iterator
        self.video_id = video_id
        self.video_feature = video_feature
        self.caption = caption
        self.input_caption_idx = input_caption_idx
        self.target_caption_idx = target_caption_idx
        self.caption_mask = mask

    def build_inputs_from_frames(self):  # TODO not used. Need to modify.
        """
        Build input pipeline that consumes TFRecords which contains frames and captions.
        :return:
        """
        def parse_frames_tfrecord(example_proto):
            """Parse the tfrecords that contain features and captions. Used in the map in Dataset below."""
            context, sequence = tf.parse_single_sequence_example(
                example_proto,
                context_features={
                    "video/id": tf.FixedLenFeature([1], dtype=tf.int64),
                },
                sequence_features={
                    "video/frames": tf.FixedLenSequenceFeature([], dtype=tf.string),
                    "video/caption": tf.FixedLenSequenceFeature([], dtype=tf.string),
                    "video/caption_idx": tf.FixedLenSequenceFeature([], dtype=tf.int64)
                }
            )

            vid = context["video/id"]
            frames = sequence["video/frames"]  # The first axis is 10, because 10 frames for 1 video
            cap = sequence["video/caption"]
            cap_idx = sequence["video/caption_idx"]

            input_length = tf.expand_dims(tf.add(tf.shape(cap)[0], 1), 0)
            input_cap = tf.slice(cap_idx, [0], input_length)
            target_cap = tf.slice(cap_idx, [1], input_length)

            return vid, frames, cap, input_cap, target_cap

        def decode_image(vid, frames, cap, input_cap, target_cap):
            decoded_frames = list()
            for idx in range(self.config["frames_per_video"]):
                frame = frames[idx]
                frame = tf.image.decode_jpeg(frame, channels=3)
                frame = tf.image.convert_image_dtype(frame, dtype=tf.float32)
                frame = tf.image.resize_images(
                            frame,
                            size=[346, 346],
                            method=tf.image.ResizeMethod.BILINEAR)
                frame = tf.subtract(frame, 0.5)
                frame = tf.multiply(frame, 2.0)
                frame = tf.expand_dims(frame, axis=0)
                decoded_frames.append(frame)

            frames = tf.concat(decoded_frames, axis=0)  # 10 x 346 x 346 x3

            return vid, frames, cap, input_cap, target_cap

        if self.config['mode'] == 'train':
            file_names = tf.gfile.Glob(
                            os.path.join(
                                self.config["tfrecord_path"], 
                                self.config['train_input_file_pattern']))
        elif self.config["mode"] == "val":
            file_names = tf.gfile.Glob(
                            os.path.join(
                                self.config["tfrecord_path"],
                                self.config["val_input_pattern"]))

        dataset = tf.data.TFRecordDataset(file_names)
        dataset = dataset.map(parse_frames_tfrecord)  # Parse TFRecord.
        dataset = dataset.map(decode_image)
        dataset = dataset.padded_batch(
            batch_size=self.config['batch_size'],
            padded_shapes=(
                tf.TensorShape([1]),  # 对应video_id
                tf.TensorShape([10, 346, 346, 3]),  # ? x 10 x 346 x 346 x3
                tf.TensorShape([None]),  # 对应caption，长度不固定
                tf.TensorShape([None]),
                tf.TensorShape([None]),
                tf.TensorShape([None])))  # 对应caption_idx，长度不固定
        iterator = dataset.make_initializable_iterator()
        video_id, video_frames, caption, \
            input_cap, target_cap, mask = iterator.get_next()

        self.iterator = iterator
        self.video_id = video_id
        self.frames = video_frames
        self.caption = caption
        self.input_caption_idx = input_cap
        self.caption_mask = mask
        self.target_caption_idx = target_cap

    def build_word_embedding(self):
        with tf.device("/cpu:0"), tf.variable_scope('word_embedding'):
            embedding_map = tf.get_variable(
                name="map",
                shape=[self.config["voc_size"], self.config["word_emb_dim"]],
                initializer=self.initializer)
            seq_embedding = tf.nn.embedding_lookup(embedding_map, self.input_caption_idx)

        self.seq_embedding = seq_embedding

    def build_video_embedding_from_frames(self):
        """Encode frames to features and build embeddings."""
        concated = list()
        with tf.variable_scope(tf.get_variable_scope(), reuse=tf.AUTO_REUSE):
            for idx in range(10):
                inception_output = video_embedding.inception_v3(
                        self.frames[:, idx, :],
                        trainable=self.config['train_inception'],
                        is_training=(self.mode == 'train'))

                concated.append(inception_output)
            
        concated = tf.concat(concated, axis=1)
        self.concated_frame_feature = concated

        self.inception_variables = tf.get_collection(
                tf.GraphKeys.GLOBAL_VARIABLES, scope="InceptionV3")

        with tf.variable_scope("video_embedding") as scope:
            video_embeddings = tf.contrib.layers.fully_connected(
                inputs=concated,
                num_outputs=self.config['video_emb_dim'],
                activation_fn=None,
                weights_initializer=self.initializer,
                biases_initializer=None,
                scope=scope)
        self.video_emb = video_embeddings

    def setup_inception_initializer(self):
        if self.config["mode"] != "inference":
            saver = tf.train.Saver(self.inception_variables)

            def restore_fn(sess):
                tf.logging.info("Restoring Inception variables from checkpoint file %s",
                        self.config["inception_checkpoint_file"])
                saver.restore(sess, self.config["inception_checkpoint_file"])
            self.init_fn = restore_fn

    def build_video_embedding_from_feature(self):
        with tf.variable_scope('video_embedding'):
            self.video_emb_W = tf.Variable(
                tf.truncated_normal(
                    shape=tf.TensorShape([self.config['video_feature_dim'], self.config['lstm_num_units']]),
                    stddev=1.0/math.sqrt(float(self.config['video_feature_dim']))),
                name='video_emb_W')
            self.video_emb_b = tf.Variable(
                initial_value=tf.zeros(tf.TensorShape(self.config['lstm_num_units'])),
                name='video_emb_b')

            self.video_emb = tf.matmul(self.video_feature, self.video_emb_W) + self.video_emb_b

    def build_model(self):
        with tf.variable_scope("lstm", initializer=self.initializer) as lstm_scope:
            self.lstm_cell = tf.contrib.rnn.BasicLSTMCell(self.config['lstm_num_units'])
            if self.mode == "train":
                self.lstm_cell = tf.contrib.rnn.DropoutWrapper(
                                    self.lstm_cell,
                                    input_keep_prob=self.config["lstm_dropout_keep_prob"],
                                    output_keep_prob=self.config["lstm_dropout_keep_prob"])

            if self.mode == "inference":
                zero_state = self.lstm_cell.zero_state(batch_size=self.config["inference_batch_size"], dtype=tf.float32)
                _, initial_state = self.lstm_cell(self.video_emb, zero_state)
                lstm_scope.reuse_variables()

                # In inference mode, run lstm step by step in order to beam search.
                tf.concat(axis=1, values=initial_state, name="initial_state")

                state_feed = tf.placeholder(dtype=tf.float32,
                                            shape=[None, sum(self.lstm_cell.state_size)],
                                            name="state_feed")
                state_tuple = tf.split(value=state_feed, num_or_size_splits=2, axis=1)

                lstm_outputs, state_tuple = self.lstm_cell(
                    inputs=tf.squeeze(self.seq_embedding, axis=[1]),
                    state=state_tuple
                )

                tf.concat(values=state_tuple, axis=1, name="state")
            else:
                zero_state = self.lstm_cell.zero_state(
                        batch_size=self.config["batch_size"],
                        dtype=tf.float32)
                _, initial_state = self.lstm_cell(self.video_emb, zero_state)

                lstm_scope.reuse_variables()

                sequence_length = tf.reduce_sum(self.caption_mask, 1)
                lstm_outputs, _ = tf.nn.dynamic_rnn(self.lstm_cell,
                                                    inputs=self.seq_embedding,
                                                    sequence_length=sequence_length,
                                                    initial_state=initial_state,
                                                    dtype=tf.float32)

        lstm_outputs = tf.reshape(lstm_outputs, [-1, self.lstm_cell.output_size])

        with tf.variable_scope("logits") as logits_scope:
            logits = tf.contrib.layers.fully_connected(
                inputs=lstm_outputs,
                num_outputs=self.config["voc_size"],
                activation_fn=None,
                weights_initializer=self.initializer,
                scope=logits_scope
            )

        if self.mode == "inference":
            tf.nn.softmax(logits, name="softmax")
        else:
            targets = tf.reshape(self.target_caption_idx, [-1])
            weights = tf.to_float(tf.reshape(self.caption_mask, [-1]))

            # Compute losses.
            losses = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=targets,
                                                                    logits=logits)
            batch_loss = tf.div(tf.reduce_sum(tf.multiply(losses, weights)),
                                tf.reduce_sum(weights),
                                name="batch_loss")
            tf.losses.add_loss(batch_loss)
            total_loss = tf.losses.get_total_loss()

            tf.summary.scalar(name="batch_loss", tensor=total_loss)
            for var in tf.trainable_variables():
                tf.summary.histogram("parameters/" + var.op.name, var)

            self.total_loss = total_loss  # Coherence loss only!

            self.target_cross_entropy_losses = total_loss
            # To print the generated sentence when training.
            generated_caption = tf.nn.softmax(logits)
            generated_caption = tf.reshape(generated_caption, [self.config["batch_size"], -1, self.config["voc_size"]])
            generated_caption = tf.argmax(generated_caption, axis=2)
            self.generated_caption = generated_caption

    def setup_global_step(self):
        """Sets up the global step Tensor."""
        global_step = tf.Variable(initial_value=0,
                                  name="global_step",
                                  trainable=False,
                                  collections=[tf.GraphKeys.GLOBAL_STEP, tf.GraphKeys.GLOBAL_VARIABLES])

        self.global_step = global_step

    def build(self):
        if self.config["input_mode"] == "frames":
            self.build_inputs_from_frames()
            self.build_word_embedding()
            self.build_video_embedding_from_frames()
            self.build_model()
            self.setup_inception_initializer()
            self.setup_global_step()
        if self.config["input_mode"] == "feature":
            self.build_inputs_from_feature()
            self.build_word_embedding()
            self.build_video_embedding_from_feature()
            self.build_model()
            self.setup_inception_initializer()
            self.setup_global_step()
