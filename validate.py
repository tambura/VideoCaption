import ipdb
import os
import tensorflow as tf
import sys
import time
import run_inference
import argparse
import yaml
sys.path.append("/home/jiangchen/data/coco-caption")
from pycocotools.coco import COCO
from pycocoevalcap.eval import COCOEvalCap

def inference(config):
    """
    return: The filename of generated captions."
    """
    return run_inference.main(config)


def score(annotation, generated):

    cocoAnno = COCO(annotation)
    cocoRes = cocoAnno.loadRes(generated)

    cocoEval = COCOEvalCap(cocoAnno, cocoRes)

    return cocoEval.evaluate()



"""
def save_model():
    if score > :
        shutil.copy()
"""


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", type=str)
    args = parser.parse_args()

    with open(args.config) as f:
        config = yaml.load(f)

    """
    start = time.time()
    # generated = inference(config)
    score(config["annotation"], "generated_caption_56159.json")
    end = time.time()
    print "Validate spend time: %s"%(end - start)
    """

    fs = os.listdir('.')
    fs = sorted([f for f in fs if f[0:17] == "generated_caption"])
    ipdb.set_trace()
    with open("filted_scores.txt", 'a') as t:
        for f in fs:
            scores = score(config["annotation"], f)
            t.write("Current file: {}".format(f))
            t.write("{}".format(scores))
            
    """

    while True:
        with open(os.path.join(config["checkpoint_path"], "checkpoint")) as f:
            ckpt_id = f.readline().split('-')[-1][:-2]
        if os.path.exists("generated_caption_{}.json".format(ckpt_id)):
            break  # Train terminate.

        generated = inference(config)
        score(config["annotation"], generated)
    """
