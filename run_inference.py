import ipdb
import os
import yaml
import json
import h5py
import tensorflow as tf
import numpy as np
from utils import inference_wrapper
from utils import caption_generator
from utils import vocabulary
import argparse


tf.logging.set_verbosity(tf.logging.INFO)


def main(config):
    # Build the inference graph.
    g = tf.Graph()
    with g.as_default():
        model = inference_wrapper.InferenceWrapper()
        restore_fn = model.build_graph_from_config(config,
                                                   config["checkpoint_path"])
    g.finalize()
    
    with open(os.path.join(config["checkpoint_path"], "checkpoint")) as f:
        ckpt_id = f.readline().split('-')[-1][:-2]

    # Create the vocabulary.
    vocab = vocabulary.Vocabulary(config["word_count_output_file"])

    # filenames = []
    # for file_pattern in args.input_files.split(","):
    #     filenames.extend(tf.gfile.Glob(file_pattern))
    # tf.logging.info("Running caption generation on %d files matching %s",
    #                 len(filenames), args.input_files)

    with tf.Session(graph=g) as sess:
        # Load the model from checkpoint.
        restore_fn(sess)

        # Prepare the caption generator. Here we are implicitly using the default
        # beam search parameters. See caption_generator.py for a description of the
        # available beam search parameters.
        generator = caption_generator.CaptionGenerator(model, vocab)

        if config["input_feature_mode"] == "hdf5":
            feature_file = h5py.File(config["feature_file"], 'r')
            generated = list()
            for video_id in range(config["test_start_id"], config["test_end_id"]):
                feature = feature_file[str(video_id)][()]
                if config["mean_feature"]:
                    feature = np.mean(feature, axis=0)
                captions = generator.beam_search(sess,
                                                 feature,
                                                 mode="feature")
                # Print the generated captions.
                # print("Captions for video {}:".format(video_id))
                # for i, caption in enumerate(captions):
                #     # Ignore begin and end words.
                #     sentence = [vocab.id_to_word(w) for w in caption.sentence[1:-1]]
                #     sentence = " ".join(sentence)
                #     print("  %d) %s (p=%f)" % (i, sentence, math.exp(caption.logprob)))
                sentence = [vocab.id_to_word(w) for w in captions[0].sentence[1:-1]]
                sentence = ' '.join(sentence)
                generated.append(
                        {"image_id": video_id,
                         "caption": sentence})
                # tf.logging.info("Wrote video {} to output file.".format(video_id))

            feature_file.close()

        elif config["input_feature_mode"] == "npy":
            generated = list()
            for video_id in range(config["test_start_id"], config["test_end_id"]):
                feature = np.load(
                            os.path.join(config["test_feature_root"],
                                         "video{:04d}.npy".format(video_id)))
                feature = np.reshape(feature, [-1])
                captions = generator.beam_search(sess,
                                                 feature,
                                                 mode="feature")
                sentence = [vocab.id_to_word(w) for w in captions[0].sentence[1:-1]]
                sentence = ' '.join(sentence)
                generated.append(
                        {"image_id": video_id,
                         "caption": sentence})
                tf.logging.info("Wrote video {} to output file.".format(video_id))

        with open(config["generated_captions"].format(ckpt_id), 'w') as t:
            json.dump(generated, t)

    return config["generated_captions"].format(ckpt_id)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", type=str)
    args = parser.parse_args()


    with open(args.config) as f:
        config = yaml.load(f)
    main(conifg)
