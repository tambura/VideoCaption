# --*-- coding: utf-8 --*--
import os
import tensorflow as tf
import numpy as np
import shutil
from caption_model import CaptionModel
from utils import vocabulary
import yaml
import argparse


parser = argparse.ArgumentParser()
parser.add_argument("--config", type=str)
args = parser.parse_args()

with open(args.config) as stream:
    config = yaml.load(stream)

tf.logging.set_verbosity(tf.logging.INFO)


def main():
    assert config['train_input_file_pattern'], "config: train_input_file_pattern is required"
    assert config["train_dir"], "config: train_dir is required"

    # Create training directory.
    train_dir = config["train_dir"]
    if not tf.gfile.IsDirectory(train_dir):
        tf.logging.info("Creating training directory: %s", train_dir)
        tf.gfile.MakeDirs(train_dir)

    # Build the TensorFlow graph.
    g = tf.Graph()
    with g.as_default():
        # Build the model.
        model = CaptionModel(config, mode='train')
        model.build()

        # Set up the learning rate.
        learning_rate_decay_fn = None
        learning_rate = tf.constant(config['initial_learning_rate'])
        if config['learning_rate_decay_factor'] > 0:
            num_batches_per_epoch = (config['num_examples_per_epoch'] /
                                     config['batch_size'])
            decay_steps = int(num_batches_per_epoch *
                              config['num_epochs_per_decay'])

            def _learning_rate_decay_fn(learning_rate, global_step):
                decayed_lr = tf.train.exponential_decay(
                    learning_rate,
                    global_step,
                    decay_steps=decay_steps,
                    decay_rate=config['learning_rate_decay_factor'],
                    staircase=True)
                tf.summary.tensor_summary(name="decayed_learning_rate", tensor=decayed_lr)
                return decayed_lr

            learning_rate_decay_fn = _learning_rate_decay_fn

        # Set up the training ops.
        train_op = tf.contrib.layers.optimize_loss(
            loss=model.total_loss,
            global_step=model.global_step,
            learning_rate=learning_rate,
            optimizer=config['optimizer'],
            summaries=['gradients'],
            clip_gradients=config['clip_gradients'],
            learning_rate_decay_fn=learning_rate_decay_fn)

        init_op = tf.local_variables_initializer()
        table_init_op = tf.tables_initializer()

        # Set up the Saver for saving and restoring model checkpoints.
        saver = tf.train.Saver(max_to_keep=config['max_checkpoints_to_keep'])

    # Run training.
    tf.contrib.slim.learning.train(
        train_op,
        train_dir,
        log_every_n_steps=config["log_every_n_steps"],
        graph=g,
        global_step=model.global_step,
        number_of_steps=config["num_steps"],
        local_init_op=tf.group(model.iterator.initializer, init_op, table_init_op),
        save_summaries_secs=600,
        # init_fn=model.init_fn,
        saver=saver,
        save_interval_secs=150)

    # Backup the configuration.
    shutil.copy("config.yaml", os.path.join(config["train_dir"], "config.yaml"))


def test():
    """Test the input of the model.
       Print some examples of input that parsed from TFRecords.
    """
    assert config['train_input_file_pattern'], "config: train_input_file_pattern is required"
    assert config["train_dir"], "config: train_dir is required"

    # Create training directory.
    train_dir = config["train_dir"]
    if not tf.gfile.IsDirectory(train_dir):
        tf.logging.info("Creating training directory: %s", train_dir)
        tf.gfile.MakeDirs(train_dir)

    voc = vocabulary.Vocabulary(config["word_count_output_file"])

    # Build the TensorFlow graph.
    g = tf.Graph()
    with g.as_default():
        # Build the model.
        model = CaptionModel(
                    config,
                    mode='train')
        model.build()

        with tf.Session() as sess:
            sess.run(tf.initialize_all_variables())
            sess.run(model.iterator.initializer)

            v_id, cap, in_cap_idx, tar_cap_idx, mask, lstm_out = sess.run(
                [model.video_id,
                 model.caption,
                 model.input_caption_idx,
                 model.target_caption_idx,
                 model.caption_mask,
                 model.to_print])
            for i in range(1):
                print("Video: {}".format(int(v_id[i])))
                print("Caption in record: {}".format(cap[i]))
                print("Input caption in index: {}".format(in_cap_idx[i]))
                print("Target caption in index: {}".format(tar_cap_idx[i]))
                print("Mask: {}".format(mask[i]))
                caption = list()
                for j in range(in_cap_idx.shape[1]):
                    caption.append(voc.id_to_word(int(in_cap_idx[i][j])))
                print("Caption converted from caption idx:")
                print(caption)

                print("LSTM output: ", lstm_out)


if __name__ == '__main__':
    main()
    # test()
